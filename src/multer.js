const multer = require('multer')

// stores file on disk
const fileStorage = multer.diskStorage({
    destination: 'images',
    filename : (req,file,cb)=>{
        cb(null,file.originalname)
    },
    fileFilter : (req,file,cb) =>{
        if (
            file.mimetype === 'image/png' ||
            file.mimetype === 'image/jpg' ||
            file.mimetype === 'image/jpeg'
        ){
            cb(null,true)
        } else{
            cb(null,false)
        }
    }
})

const fileFilter = (req,file,cb) =>{
    if (
        file.mimetype === 'image/png' ||
        file.mimetype === 'image/jpg' ||
        file.mimetype === 'image/jpeg'
    ){
        cb(null,true)
    } else{
        cb(null,false)
    }
}

const uploads = multer({storage: fileStorage,fileFilter:fileFilter})

module.exports = uploads

// const uploads = multer({
//     limits:{
//         fileSize: 1000000
//     },
//     fileFilter(req,file,cb){
//         if(!file.originalname.match(/\.(jpg|jpeg|png|jfif)$/)){
//             cb(new Error('plz,upload image'))
//         }
//         cb(null,true)
//     }
// })

// module.exports = uploads









// const fileStorage = multer.diskStorage({
//     destination: 'images',
//     filename : (req,file,cb)=>{
//         cb(null,file.originalname)
//     }
// })

// const fileFilter = (req,file,cb) =>{
//     if (
//         file.mimetype === 'image/png' ||
//         file.mimetype === 'image/jpg' ||
//         file.mimetype === 'image/jpeg'
//     ){
//         cb(null,true)
//     } else{
//         cb(null,false)
//     }
// }