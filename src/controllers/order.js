const Point = require('../models/point')
const Order = require('../models/order')

const addOrder = async(req,res)=>{
    try{
        const phone = req.body.phone
        const userPoint = await Point.find({phone:phone})
        if(!(userPoint[0])){
            const point = new Point({
                name: req.body.name,
                phone: req.body.phone,
                location: req.body.location,
                // point: req.body.point,
               
            })
            await point.save()
        }
        // if(userPoint[0]){
        //     const R = userPoint[0].point + req.body.point
        //     userPoint[0].point = R
        //     await userPoint[0].save()
        // } 
        const order = new Order({
            name: req.body.name,
            phone: req.body.phone,
            location: req.body.location,
            orderData: req.body.orderData,
            point: req.body.point,
            notes: req.body.notes,
            payment: req.body.payment,
            delivery: req.body.delivery,
            deliveryPrice: req.body.deliveryPrice,
            totalPrice: req.body.totalPrice,
        })
        await order.save()
        res.status(200).send(order)


    }
    catch(e){
        res.status(400).send(e)
    }
}


const allOrder = async(req,res)=>{
    try{
        const order = await Order.find()
        if(order.length === 0){
            return res.status(200).send(order)
        }
        res.status(200).send(order)
    }
    catch(e){
        res.status(400).send(e)
    }
}

const SpecificOrder = async(req,res)=>{
    try{
        const order = await Order.find({StatusEdit:req.body.StatusEdit})
        if(order.length === 0){
            return res.status(200).send(order)
        }
        res.status(200).send(order)
    }
    catch(e){
        res.status(400).send(e)
    }
}


const GetStatus = async(req,res)=>{
    try{
        if(req.body.delivery){
            const Filter = await Order.find({payment:req.body.payment, delivery:req.body.delivery, StatusEdit:'Done'})
            return res.status(200).send(Filter)
        }
        const Filter = await Order.find({payment:req.body.payment,StatusEdit:'Done'})
        // if(Filter.length === 0){
        //     return res.status(200).send(Filter)
        // } 
        res.status(201).send(Filter)
    }
    catch(e){
        res.status(400).send(e)
    }
}

const editOrder = async(req,res)=>{
    const updata = Object.keys(req.body)
    const alloweUpdatas = ['StatusEdit']

    let isValid = updata.every((el)=>alloweUpdatas.includes(el))
     if(!isValid){
         return res.status(400).send("Can't updata plz, cheack your edit key")
     }

    try{    
        const _id = req.params.id
        const order = await Order.findById(_id)
        if(!order){
            return res.status(200).send([])
        }
        if(req.body.StatusEdit==='Done'){
            const userPoint = await Point.find({phone:order.phone})
            if(userPoint[0]){
                userPoint[0].point = userPoint[0].point + order.point
                await userPoint[0].save()
            }
        }
        
        updata.forEach((el)=>(order[el]=req.body[el]))
        await order.save()
        res.status(200).send(order)
    }
    catch(e){
        console.log(e);
        res.status(400).send(e)
    }
}



const deleteOrder = async(req,res)=>{
    try{
        const _id = req.params.id
        const order = await Order.findById(_id)
        if(!order){
            return res.status(200).send([])
        }
        await Order.findByIdAndDelete(_id)
        res.status(200).send("Delete Sucssfly")
    }
    catch(e){
        res.status(400).send(e)
    }
}


module.exports = {
    addOrder,
    allOrder,
    editOrder,
    deleteOrder,
    SpecificOrder,
    GetStatus
}