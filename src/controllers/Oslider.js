const Slider = require('../models/Oslider')
const cloud = require('../cloudinary')
// const images = require('../../images')
const fs = require('fs')

// const addSlider = async(req,res,cb)=>{
//     try{
//         // const result = await cloud.uploads(req.files[0].path)
//         const path = req.file.path 
//         const img = fs.readFileSync(path);

//         const imageBuffer = await Buffer.from(img).toString('base64');
//         const sliderImage = new Slider ({
//             // imageUrl:result.url
//             imageUrl:imageBuffer
//         })
//         await sliderImage.save()
//         fs.unlinkSync(req.file.path)
//         res.status(200).send(sliderImage)
//     }
//     catch(e){
//         res.status(400).send(e)
//     }
// }

const addSlider = async(req,res,cb)=>{
    try{
        const sliderImage = new Slider ({
            name: req.body.name,
            image: req.body.image
        })
        await sliderImage.save()
        res.status(200).send(sliderImage)
    }
    catch(e){
        res.status(400).send(e)
    }
}


const allSlider = async(req,res)=>{
    try{
        const sliderImage = await Slider.find()
        if(sliderImage.length === 0){
            return res.status(200).send(sliderImage)
        }
        res.status(200).send(sliderImage)
    }
    catch(e){
        res.status(400).send(e)
    }
}

// const editSlider = async(req,res)=>{
//     const updata = Object.keys(req.body)
//     const alloweUpdatas = ['imageUrl','name']

//     let isValid = updata.every((el)=>alloweUpdatas.includes(el))
//      if(!isValid){
//          return res.status(400).send("Can't updata plz, cheack your edit key")
//      }

//     try{
//         if(req.files[0].path){
//             const result = await cloud.uploads(req.files[0].path)

//             const _id = req.params.id
//             const slider = await Slider.findById(_id)
//         }

//         if(!slider){
//             return res.status(404).send('No image found')
//         }
        
//         slider.imageUrl = result.url
//         await slider.save()
//         res.status(200).send(slider)
//     }
//     catch(e){
//         console.log(e);
//         es.status(400).send(e)
//     }
// }



const deleteSlider = async(req,res)=>{
    try{
        const _id = req.params.id
        const slider = await Slider.findById(_id)
        if(!slider){
            return res.status(200).send([])
        }
        await Slider.findByIdAndDelete(_id)
        res.status(200).send("Delete Sucssfly")
    }
    catch(e){
        res.status(400).send(e)
    }
}


module.exports = {
    addSlider,
    allSlider,
    // editSlider,
    deleteSlider
}