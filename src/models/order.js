const mongoose = require('mongoose')
const validator = require('validator')
const opts = {
    // Make Mongoose use Unix time (seconds since Jan 1, 1970)
    
    timestamps: { currentTime: () =>new Date().setHours(new Date().getHours() + 2) }
};

const OrderSchema = mongoose.Schema({
    name:{
        type: String,
        required: true,
    },
    phone:{
        type: String,
        required: true,
        validate(phone){
            if(!validator.isMobilePhone(phone,'ar-EG')){
                throw new Error('phone is invalid')
            }
        }
    },
    location:{
        type: String,
        required: true,
        default:' '
    },
    orderData:[{
        type: String,
        required: true,
    }],
    notes:{
        type: String,
        default:' '
    },
    point:{
        type: Number,
        default:0
    },
    StatusEdit:{
        type: String,
        default: 'Order'
    },
    payment:{
        type: String,
        required: true,
        default:'cash'
    },
    delivery:{
        type: String,
        required: true,
        default:'takway'
    },
    deliveryPrice:{
        type: Number,
        default:0
    },
    totalPrice:{
        type: Number,
        required: true,
        default:0
    },

},
opts
)





// OrderSchema.methods.toJSON = function(){
//     // document
//     const Order = this

//     // Converts this document into a object
//     const OrderObject = Order.toObject()

    

//     return OrderObject

// }

const Order = mongoose.model('Order',OrderSchema)

module.exports = Order