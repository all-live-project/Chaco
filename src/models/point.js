const mongoose = require('mongoose')
const validator = require('validator')

const opts = {
    // Make Mongoose use Unix time (seconds since Jan 1, 1970)
    
    timestamps: { currentTime: () =>new Date().setHours(new Date().getHours() + 2) }
};

const PointSchema = mongoose.Schema({
    name:{
        type: String,
        required: true,
    },
    phone:{
        type: String,
        required: true,
        validate(phone){
            if(!validator.isMobilePhone(phone,'ar-EG')){
                throw new Error('phone is invalid')
            }
        }
    },
    location:{
        type: String,
        default:' '
    },
    point:{
        type: Number,
        default:0
    },
    StatusEdit:{
        type: Boolean,
        default: false
    }

},
opts
)



PointSchema.methods.toJSON = function(){
    // document
    const point = this

    // Converts this document into a object
    const pointObject = point.toObject()

    

    return pointObject

}

const Point = mongoose.model('Point',PointSchema)

module.exports = Point