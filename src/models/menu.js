const mongoose = require('mongoose')

const opts = {
    // Make Mongoose use Unix time (seconds since Jan 1, 1970)
    
    timestamps: { currentTime: () =>new Date().setHours(new Date().getHours() + 2) }
};

const MenuSchema = mongoose.Schema({
    name:{
        type: String,
        required: true,
    },
    desc:{
        type: String,

    },
    image:{
        type: String,
        default:''
    },
    imageUrl:{
        type: String,
        default:''
    },
    price:{
        type: Number,
        required: true,
    },
    isAvailable:{
        type: Boolean,
        default: true
    },
    point:{
        type: Number,
        default:0
    },
    CategoryName:{
        type: String,
        required: true
    },
    AverageReview:{
        type: Number,
        default:0
    },
    review:[{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Review'
    }],
    admin:{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref:'Admin'
    },
    // date:{
    //     type: String,
    //     default: Date.now()
    // }
},
opts
)

// MenuSchema.statics.findAndUpdateReview = async(mealID,reviewID)=>{
//     meal = this
//     // const meal = await Menu.findOne({mealID})
//     // if(!meal){
//     //     throw new Error('No meal found')
//     // }
//     // await meal.review.concat(reviewID)
//     meal.review = meal.review.concat(reviewID)
//     await meal.save()
//     return meal
// }



MenuSchema.methods.toJSON = function(){
    // document
    const Menu = this

    // Converts this document into a object
    const MenuObject = Menu.toObject()

    

    return MenuObject

}

const Menu = mongoose.model('Menu',MenuSchema)

module.exports = Menu