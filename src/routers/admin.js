const express = require('express')
const auth = require('../middelware/auth')
const router = new express.Router()
const adminControllers = require('../controllers/admin')
const uploads = require('../multer')

router.post('/Addadmin',auth,adminControllers.Addadmin)

router.post('/login',adminControllers.adminLogin)

// router.get('/usersProfile',auth,adminControllers.userProfile)

router.patch('/admins/:id',auth,adminControllers.adminUpdate)

router.delete('/logout',auth,adminControllers.adminLogout)

// router.delete('/logoutall',auth,adminControllers.adminLogoutAll)

router.delete('/admins/:id',auth,adminControllers.adminDelete)

// router.post('/admins/image',auth,uploads.single('image'),adminControllers.adminImage)



module.exports = router
