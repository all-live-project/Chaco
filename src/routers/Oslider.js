const express = require('express') 
const auth = require('../middelware/auth')
const router = new express.Router()
const sliderControllers = require('../controllers/Oslider')
const uploads = require('../multer')


// router.post('/addSlider',auth,uploads.single('image'),sliderControllers.addSlider)  

router.post('/addSlider',auth ,sliderControllers.addSlider)

router.get('/allSlider',sliderControllers.allSlider)

// router.patch('/editSlider/:id',auth,sliderControllers.editSlider)

router.delete('/deleteSlider/:id',auth,sliderControllers.deleteSlider)




module.exports = router